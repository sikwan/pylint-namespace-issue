from setuptools import setup, find_packages

VERSION = "1.0.0"

long_description = "Test"

setup(
    name="company.pkg1",
    version=VERSION,
    description="Test",
    long_description=long_description,
    author="Francois Vanderkelen",
    license="GPL",
    install_requires=[],
    packages=find_packages(),
    namespace_packages=[
        'company'
    ],
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Intended Audience :: Developers',
        'Operating System :: Unix',
        'Programming Language :: Python'
    ]
)
