class BaseModel(object):
    def __init__(self):
        self._uid = 0

    def first_function(self):
        self.uid = 1

    def second_function(self):
        self.uid = 2

    @property
    def uid(self):
        return self._uid

    @uid.setter
    def uid(self, value):
        self._uid = value
