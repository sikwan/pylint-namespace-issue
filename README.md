# README #

### What is this repository for? ###

* Provides a sample related to a pylint bug.
* 1.0

### How to use? ###

* Download sources
* Create a virtualenv
* install pylint
* install packages
* run pylint against pkg2
    * This will show import-error
* install sources outside virtualenv
* run pylint against pkg2
    * This will work
